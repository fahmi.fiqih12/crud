<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use DB;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = DB::table('questions')->get();
        return view('question.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('question.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dump($request->all());
         
        $request->validate([
            'judul' => 'required|unique:question',
            'isi' => 'required'
        ]);

        $query = DB::table('questions')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        
        return redirect('/question/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // menggunakan query builder
        $questions = DB::table('questions')->where('id', $id)->get();
        return view('question.show', compact($questions));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        $questions = DB::table('questions')->where('id', $id)->first();
        return view('question.edit', compact($questions));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $request->validate([
            'judul' => 'required|unique:question',
            'isi' => 'required'
        ]);
        
        $query = DB::table('questions')
                            ->where('id',$id)
                            ->update([
                                'judul' => $request['judul'],
                                'isi' => $request['isi']
                            ]);
        return redirect('/question'->with('success','Berhasil update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        //
    }
}
