<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikeAnswaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_answares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('answare_id');
            $table->unsignedBigInteger('profil_id');
            $table->foreign('answare_id')->references('id')->on('answares');
            $table->foreign('profil_id')->references('id')->on('profiles');
            $table->integer('poin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_answares');
    }
}
