@extends('layout.main')

@section('form')
  <div class="container">
    <div class="row">
      <div class="md-10 my-3">
        <div class="card" style="width: 18rem;">
          <div class="card-body">
          <form action="/question/{{ $question->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
              <label for="judul">judul</label>
              <input type="text" class="form-control" id="judul" name="judul"   placeholder="masukan judul">              
              @error('judul')
               <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="isi">isi</label>
              <input type="text" class="form-control" id="isi" name="isi" placeholder="masukan isi">
               @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
               @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection