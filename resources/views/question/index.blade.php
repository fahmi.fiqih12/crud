@extends('layout.main')

@section('table')

<div class="container">
  <div class="row">
    <div class="col-10">
      <table class="table my-3">
        <thead class="thead-dark">
          <tr>
            <th scope="col">NO</th>
            <th scope="col">judul</th>
            <th scope="col">Isi</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse($questions as $question)
          <tr>
            <th scope="row">{{ $loop->iteration}}</th>
            <td>{{ $question->judul }}</td>
            <td>{{ $question->isi}}</td>
            <td>
              <a href="/questions->{{$question->id}}" class="badge badge-primary">Show</a>
              <a href="/questions->{{$question->id}}/edit" class="badge badge-success">edit</a>
              <a href="" class="badge badge-danger">hapus</a>
            </td>
          </tr
          @empty
            <tr>
              <td colspan="4" align="center" color="red">No Post</td>
            </tr>
          @endforelse
        </tbody>
      </table>

    </div>
  </div>
</div>

@endsection