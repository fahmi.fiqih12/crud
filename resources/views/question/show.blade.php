@extends('layout.main')

@section('show')
<div class="container">
  <h3>{{ $question->judul }}</h3>
  <p>{{ $question->isi }}</p>
</div>
@endsection