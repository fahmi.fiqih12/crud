<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('welcome');
});


Route::get('/question/create', 'QuestionsController@create');
Route::post('/question', 'QuestionsController@store');
// get/read
Route::get('/question', 'QuestionsController@index');
// show
Route:get('question/{id}', 'QuestionsController@show');
Route::get('question/{id}/edit', 'QuestionsController@edit');
Route::get('question/{id}', 'QuestionsController@update');


